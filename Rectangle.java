import tinshoes.input.SafeScanner;
import java.util.Scanner;
public class Rectangle {
	static String err = " is not valid input.";
	public static void main(String[] args) {
		Scanner usc = new Scanner(System.in);
		SafeScanner sc = new SafeScanner(usc);
		System.out.println("Enter points to test if they are inside a rectangle.");
		System.out.println(isInRect(sc.nextDouble("Rect x1:", err, true), sc.nextDouble("Rect y1:", err, true), sc.nextDouble("Rect x2:", err, true), sc.nextDouble("Rect y2:", err, true), sc.nextDouble("Point x:", err, true), sc.nextDouble("Point y:", err, true)) ? "Yes." : "No.");
		System.out.println("Enter points to test if the resulting rectangle is parallel to the x axis.");
		System.out.println(rectParallelToX(sc.nextDouble("Rect x1:", err, true), sc.nextDouble("Rect y1:", err, true), sc.nextDouble("Rect x2:", err, true), sc.nextDouble("Rect y2:", err, true), sc.nextDouble("Rect x3:", err, true), sc.nextDouble("Rect y3:", err, true), sc.nextDouble("Rect x4:", err, true), sc.nextDouble("Rect y4:", err, true)) ? "Yes." : "No.");
	}
	public static boolean isInRect(double x1, double y1, double x2, double y2, double x, double y) {
		if (x1 > x2) {
			if (y1 > y2)
				return x <= x1 && x >= x2 && y <= y1 && y >= y2;
			else
				return x <= x1 && x >= x2 && y <= y2 && y >= y1;
		} else {
			if (y1 > y2)
				return x <= x2 && x >= x1 && y <= y1 && y >= y2;
			else
				return x <= x2 && x >= x1 && y <= y2 && y >= y1;
		}
	}
	public static boolean rectParallelToX(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4) {
		double[][] points = {new double[] {x1, y1}, new double[] {x2, y2}, new double[] {x3, y3}, new double[] {x4, y4}};
		int i;
		for (i = 1; i <= 4; i++) {
			if (i == 4)
				return false;
			if (points[0][1] == points[i][1])
				break;
		}
		int j;
		for (j = 1; i <= 4; i++) {
			if (j == 4)
				return false;
			if (points[0][0] == points[j][0])
				break;
		}
		int k;
		for (k = 1; k < 4; k++) {
			if (k != i && k != j)
				break;
		}
		if (points[i][0] != points[k][0] || points[j][1] != points[k][1])
			return false;
		return true;
	}
}