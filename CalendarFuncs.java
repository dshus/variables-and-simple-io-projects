import java.lang.IllegalArgumentException;
import java.util.regex.*;
import java.util.Scanner;
import tinshoes.input.SafeScanner;
//I WROTE THIS AT 12 SO THERE ARE PROBABLY LOTS OF BUGS
public class CalendarFuncs {
	static int[] monthLengths = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	static Pattern julian = Pattern.compile("(\\d+)\\/(\\d{1,3})");
	static String errText = " is not valid input.";
	public static void main(String[] args) {
		Scanner usc = new Scanner(System.in);
		SafeScanner sc = new SafeScanner(usc);
		try {
			System.out.println("MM/DD/YYYY -> YYYY/DDD");
			System.out.println(toJulian(sc.nextInt("Enter the month.", errText, true), sc.nextInt("Enter the day.", errText, true), sc.nextInt("Enter the year.", errText, true)));
			System.out.println("YYYY/DDD -> MM/DD/YYYY\nEnter the Julian Date.");
			System.out.println(fromJulian(usc.next()));
			System.out.println("Days Between\nEnter two Julian Dates.");
			System.out.println(daysBetween(usc.next(), usc.next()));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	public static String toJulian(int month, int day, int year) {
		if (day <= 0 || month <= 0 || year <= 0)
			throw new IllegalArgumentException("There cannot be negative months, days or years.");
		if ((day > monthLengths[month - 1] && !(month == 2 && day == 28)) || year == 0)
			throw new IllegalArgumentException("The date is invalid.");
		int date = 0;
		for (int i = 0; i < (month - 1); i++)
			date += monthLengths[i];
		date += day;
		if (year % 4 == 0 && month > 2)
			date++;
		return year + "/" + date;
	}
	public static String fromJulian(String input) {
		Matcher m = julian.matcher(input);
		if (!m.find())
			throw new IllegalArgumentException("There is no valid date.");
		int year = Integer.parseInt(m.group(1));
		int date = Integer.parseInt(m.group(2));
		/*if (year < 1 && date < 1)
			throw new IllegalArgumentException("The year or date cannot be less than 1");*/ //removed because negative sign is not supported
		boolean leap = year % 4 == 0;
		if (date > 366 || (!leap && date > 365))
			throw new IllegalArgumentException("The date is too large.");
		int month = 0;
		int day = date;
		while (day > monthLengths[month]) {
			day -= monthLengths[month];
			if (leap && month == 1)
				day--;
			month++;
		}
		if (day < 1)
			day = monthLengths[month-1] + day;
		return (month+1) + "/" + day + "/" + year;
	}
	public static int daysBetween(String input1, String input2) {
		Matcher m1 = julian.matcher(input1);
		if (!m1.find())
			throw new IllegalArgumentException("There is no valid Julian date.");
		int year1 = Integer.parseInt(m1.group(1));
		int date1 = Integer.parseInt(m1.group(2));
		Matcher m2 = julian.matcher(input2);
		if (!m2.find())
			throw new IllegalArgumentException("There is no valid Julian date.");
		int year2 = Integer.parseInt(m2.group(1));
		int date2 = Integer.parseInt(m2.group(2));
		boolean leap1 = year1 % 4 == 0;
		boolean leap2 = year2 % 4 == 0;
		int yearLen1 = leap1 ? 366 : 365;
		int yearLen2 = leap2 ? 366 : 365;
		if (date1 > yearLen1 || date2 > yearLen2)
			throw new IllegalArgumentException("A date is too large.");
		int yearDiff = Math.abs(year1 - year2);
		int leapDays = 0;
		if (yearDiff != 0)
			for (int x = (year1 < year2 ? year1 : year2); x < (year1 < year2 ? year2 : year1); x++)
				if (x % 4 == 0)
					leapDays++;
		if (yearDiff == 0 && date1 < date2 ? (date1 <= 60 && date2 > 60) : (date2 <= 60 && date1 > 60))
			leapDays++;
		else {
			if (year1 > year2 ? leap1 : leap2 && year1 > year2 ? date1 > 60 : date2 > 60)
				leapDays++;
			if (year1 < year2 ? leap1 : leap2 && year1 < year2 ? date1 <= 60 : date2 <= 60)
				leapDays++;
		}
		return yearDiff == 0 ? Math.abs(date1 - date2) : yearDiff * 365 + (year1 > year2 && date1 < date2 ? (yearLen2 - date2) + date1 : (year2 > year1 && date2 > date1 ? (yearLen1 - date1) + date2 : Math.abs(date1 - date2))) + leapDays;// + (int)Math.floor(yearDiff / 4);
	}
}