import java.util.Scanner;
import java.util.HashMap;
import tinshoes.input.SafeScanner;
import tinshoes.input.Interface;
public class RunSpeedRace {
	public static int[] sortIndices(double[] input, boolean ascending) {
		double[] sortable = new double[input.length];
		for (int x = 0; x < input.length; x++) {
			sortable[x] = input[x];
		}
		int[] sorted = new int[sortable.length];
		for (int i = 0; i < sorted.length; i++) {
			sorted[i] = i;
		}
		//Exchange sort with voodoo
		double temp;
     	for (int i = 0; i < sortable.length - 1; i ++ ) {
          	for (int j = i + 1; j < sortable.length; j++) {
               if((ascending ? (sortable[i] < sortable[j]) : (sortable[i] > sortable[j]))) {
                       temp = sortable[i];
                       sortable[i] = sortable[j];
                       sortable[j] = temp;
                       temp = sorted[i];
                       sorted[i] = sorted[j];
                       sorted[j] = (int)temp; 
                }           
          	}
          	
     	}
     	return sorted;
	}
	public static double runSpeed(int weight, int height, int stride, int age) {
		if (age <= 3)
			return 0;
		int closenessTo20 = Math.abs(age - 20);
		int result = (height / 66)  * (weight / 100) * stride - (closenessTo20/10);
		return result < 0 ? 0 : result;
	}
	public static int[] race(double[] input, int distance) {
		double[] contestants = input;
		for (int i = 0; i < contestants.length; i++) {
			contestants[i] *= ((Math.random() * 0.75) + 0.25); //New speed = old speed cut by a range of 0.75 to 1.0
			contestants[i] = distance / contestants[i]; //Race time = distance / speed
		}
		return sortIndices(contestants, false);
	}
	public static void main(String[] args) {
		Scanner usc = new Scanner(System.in);
		SafeScanner sc = new SafeScanner(usc);
		String errText = " is invalid input.";
		double[] people = new double[5];
		for (int i = 0; i < people.length; i++) {
			System.out.println("Please enter person #" + (i + 1) + "'s characteristics.");
			people[i] = runSpeed(sc.nextInt("Enter your weight.", errText, true), sc.nextInt("Enter your height.", errText, true), sc.nextInt("Enter your stride.", errText, true), sc.nextInt("Enter your age.", errText, true));
		}
		int[] sortedPeople = sortIndices(people, true); //people was not modified for later use with race()
		for (int j = 0; j < sortedPeople.length; j++) {
			System.out.println("The " + (j+1) + Interface.th(j+1) + " fastest is Person #"  + (sortedPeople[j]+1) + " with a speed of " + people[sortedPeople[j]] + "ft/s.");
		}
		System.out.println("\n\nRacing...\n\n");
		int raceLength = 300; //feet
		int[] racedPeople = race(people, raceLength); //People was modified, It is now the race time.
		System.out.println("Here are the results for a " + raceLength + " ft long race:");
		for (int j = 0; j < racedPeople.length; j++) {
			System.out.println("In " + (j+1) + Interface.th(j+1) + " place is Person #"  + (racedPeople[j]+1) + " with a time of " + people[racedPeople[j]] + " seconds.");
		}
		System.out.println("     __" + (racedPeople[0]+1) + "__\n__" + (racedPeople[1]+1) + "__|1st|\n|2nd||   |__" + (racedPeople[2]+1) + "__\n|   ||   ||3rd|"); //Award Ceremony
		
	}
}