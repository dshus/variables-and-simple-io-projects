import java.util.Scanner;
import tinshoes.input.SafeScanner;
public class RunSpeedCapacity {
	public static double RunSpeed(int weight, int height, int stride, int age) {
		if (age <= 3)
			return 0;
		int closenessTo20 = Math.abs(age - 20);
		int result = (height / 66)  * (weight / 100) * stride + (closenessTo20/10);
		return result < 0 ? 0 : result;
	}
	public static void main(String[] args) {
                Scanner usc = new Scanner(System.in);
		SafeScanner sc = new SafeScanner(usc);
                String errText = " is invalid input.";
                System.out.println("The person's speed is " + RunSpeed(sc.nextInt("Enter your weight.", errText, true), sc.nextInt("Enter your height.", errText, true), sc.nextInt("Enter your stride.", errText, true), sc.nextInt("Enter your age.", errText, true)) + " feet per second. Maybe.");
	}
}
