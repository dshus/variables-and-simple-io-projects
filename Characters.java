import java.util.Scanner;

public class Characters {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		char chars[] = new char[5];
		System.out.println("Please enter 5 characters:");
		int n = 0;
		String curr;
		while (n < 5) {
			curr = sc.next();
			for (int i = 0; i < curr.length(); i++) {
				if (n == 5)
					break;
				chars[n] = curr.charAt(i);
				n++;
			}
		}
		System.out.println(chars[0] + " + " + chars[1] + " + " + chars[2] + " + " + chars[3] + " + " + chars[4] + " = " + (chars[0] + chars[1] + chars[2] + chars[3] + chars[4]) + "\nThe last character, " + chars[4] + ", has a value of " + (int)chars[4]);
	}
}