import java.util.Scanner;
import javax.swing.JOptionPane;

public class LibraryFines {
	public static String th(int x) {
		String str = Integer.toString(x);
		char f = str.charAt(str.length() - 1);
		return f == '1' ? "st" : (f == '2' ? "nd" : "th");
	}
	public static void main(String[] args) {
		String name[] = new String[3];
		try {
			name[0] = JOptionPane.showInputDialog("What is your first name?");
			name[1] = JOptionPane.showInputDialog("What is your middle name?");
			name[2] = JOptionPane.showInputDialog("What is your last name?");
		} catch (Exception e) {
			System.err.println("An error occurred. Please try again.");
		}
		for (int i = 0; i < 3; i++) {
			if (name[i] == null) {
				System.err.println("Please enter a valid name.");
				return;
			}
		}
		System.out.println("Please enter your 10 library fines.");
		Scanner sc = new Scanner(System.in);
		float min = 10.95f;
		float max = 0.0f;
		float total = 0.0f;
		float current;
		boolean canContinue;
		for (int i = 0; i < 10; i++) {
			while (true) {
				System.out.print("Enter the " + (i+1) + th(i+1) + " fine: ");
				if (sc.hasNextFloat()) {
					current = sc.nextFloat();
					if (current < 0 || current > 10.95) {
						System.err.println("The fines must range from 0 to 10.95.");
						continue;
					}
					if ((current * 100) % 1 != 0) {
						System.err.println("A fine cannot be a fraction of a cent.");
						continue;
					}
					if (current < min) min = current;
					if (current > max) max = current;
					total += current;
					break;
				} else
					System.err.println(sc.next() + " is not a valid library fine.");
			}
		}
		System.out.println("\n\n\n\nOkay, " + name[0] + " " + name[1] + " " + name[2] + ", you have a total of $" + total + " in fines due.\nYour smallest individual fine is $" + min + ", and your largest individual fine is $" + max + ".");
	}
}
