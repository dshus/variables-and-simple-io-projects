import java.util.Scanner;
import tinshoes.input.SafeScanner;
import java.lang.IllegalArgumentException;
public class Calculator {
	static final String errText = " is not valid input.";
	public static void main(String[] args) {
		Scanner usc = new Scanner(System.in);
		SafeScanner sc = new SafeScanner(usc);
		System.out.println("Decimals are allowed!");
		double in1 = sc.nextInt("Enter the 1st value.", errText, true);
		double in2 = sc.nextInt("Enter the 2nd value.", errText, true);
		System.out.println("Enter the operation: (+, -, *, /, ^)");
		String in3 = usc.next();
		try {
			System.out.println(calc(in1, in2, in3));
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
	public static double calc(double num1, double num2, String op) {
		switch (op) {
			case "+": return num1 + num2;
			case "-": return num1 - num2;
			case "*": return num1 * num2;
			case "/":
				if (num2 == 0)
					throw new IllegalArgumentException("Cannot divide by zero.");
				return num1 / num2;
			case "^": return Math.pow(num1, num2);
			default: throw new IllegalArgumentException("There is no valid operation.");
		}
	}
}